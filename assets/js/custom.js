/* onResize function*/
// Copyright (c) 2013 "chasingmaxwell" Peter Sieg. The MIT License (MIT).
function OnResize(){'use strict';var t,s=this;this.width=0;this.delay=250;this.behaviors=[];this.getWidth=function(){var width=0;if(document.body&&document.body.offsetWidth){width=document.body.offsetWidth}if(document.compatMode=='CSS1Compat'&&document.documentElement&&document.documentElement.offsetWidth){width=document.documentElement.offsetWidth}this.width=width};this.add=function(b){this.behaviors.push(b)};this.run=function(){this.getWidth();for(var i=0,len=this.behaviors.length;i<len;i++){if(typeof this.behaviors[i]=='function'){this.behaviors[i](this.width)}}};this.getWidth();window.onresize=function(){if(typeof t!=='undefined'){clearTimeout(t)}t=setTimeout(function(){s.run()},s.delay)}}

/* Slide toggle function */
$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
};

/* Global functions */
$(".i-see-features").click(function() {
	var trigger = $(this);
	trigger.parent().find('ul.features-list').slideFadeToggle();
	trigger.toggleClass("open");
	return false;
})

	
$(function() {
	$('.categoryHeader a').click(function(e) {
		var oR = new OnResize();
		windowWidth = oR.width;
		if (windowWidth < 768) {
			e.preventDefault();
			$(this).parent().next('ul').slideFadeToggle();
			$(this).toggleClass('open');	
		}								
	});
	
	$(".about-organisation h3, .organisation-products h3").click(function(e) {
		 var oR = new OnResize();
			windowWidth = oR.width;
			if (windowWidth < 768) {
				e.preventDefault();
				$(this).next().slideFadeToggle();
			}								
	});
	
	$(".business-description").click(function() {
		$(this).next("p").slideFadeToggle();
		$(this).toggleClass("open")
		return false;
	})
	
	$(".filter").click(function() {
		$(this).next(".categoryListContainer").slideFadeToggle();
		$(this).toggleClass("open")
		return false;
	})
	
	$(".login-info h2, .register-uk-directory h2").click(function() {
		$(this).next().slideToggle(500);
		$(this).toggleClass("open");
		return false;
	})

	$(".opening-box .headertitle").click(function() {
		$(this).next().slideToggle(500);
		$(this).toggleClass("open");
		return false;
	})
	$(window).load(function() {
		if ($(window).width() < 767) {
			$(".add-listing-register .login-info h2").click(function() {
				$(".add-listing-register 	.login-info  ul").slideFadeToggle();
				$(this).toggleClass("open");

			})
			$(".add-listing-register .register-uk-directory h2").click(function() {
				$(".add-listing-register .register-uk-directory ul").slideFadeToggle();
				$(this).toggleClass("open");
			})
		}
	})

	$('.search-icon').click(function() {
		$('.searchFormContainer').slideFadeToggle();
	})
	
	$(".login-icon, .nav-bar .closeMBtn").click(function() {
			//$("body").toggleClass("login-menu");
			$("body").toggleClass("move");
				if ($("body").hasClass('move')) {
					$('.wrapper').animate({
						marginLeft : '-80%'
				},500);
				$('.nav-bar').animate({
					right : '0'
				},500);
			} else {
				$('.wrapper').animate({
					marginLeft : '',
				},500);
				$('.nav-bar').animate({
					right : '-80%'
				},500);
			}
	})
});

$(window).load(function() {

	if ($(window).width() > 767) {

		$(".hide").click(function() {

			$(this).prev().slideFadeToggle();
			if ($(this).hasClass("open")) {
				$(this).removeClass("open").addClass("close");
				return false;
			} else {
				$(this).removeClass("close").addClass("open");
				return false;
			}
		})
	}
});

